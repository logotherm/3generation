Einstellungen
=============

.. image:: image/einstellungen.png

Netzwerkeinstellungen
---------------------
DHCP
^^^^
Um die Netzwerkeinstellungen des Displays automatisch von einem DHCP Server konfigurieren zu lassen, muss **DHCP** im Netzwerkmenü ausgewählt sein. Die Felder *IP*, *Subnet*, *Gateway* und *DNS* sind ausgegraut und zeigen die vom DHCP Server zugeteilten Einstellungen an.

.. image:: image/dhcp.png

Ist **STATIC** ausgewählt, so müssen Sie **DHCP** auswählen und die Eingabe bestätigen. Die Oberfläche und des Netzwerkinterface des Displays starten daraufhin neu (dies kann ein paar Sekunden dauern).

STATIC
^^^^^^
Um die Netzwerkeinstellungen des Displays manuell vorzunehmen muss **STATIC** im Netzwerkmenü ausgewählt sein. Dann können Sie die *IP*, die *Subnet Mask*, das *Gateway* und einen *DNS Server* manuell einstellen. 

.. image:: image/static.png

Tippen Sie dazu auf ein Feld um das Eingabefeld zu öffnen.

.. image:: image/static_ip.png

Wenn Sie alle Einstellungen angepasst haben müssen Sie Ihre Eingabe bestätigen. Die Oberfläche und des Netzwerkinterface des Displays starten daraufhin neu (dies kann ein paar Sekunden dauern).

Code eingeben
-------------

Hier können Sie auf eine andere Nutzerebene wechseln. Berechnen Sie hierzu mithilfe der **Zufallszahl** (neben dem Zahlenfeld) den Servicecode und geben diesen im Zahlenfeld ein.

.. image:: image/code.png

.. image:: image/code_eingegeben.png

Bestätigen Sie anschließend mit **OK**. Wenn Sie einen gültigen Code eingegeben haben wechselt das *Bottom Menü* die Farbe und es wird die Zugangsebene angezeigt (in diesem Fall 4).

.. image:: image/code_success.png
