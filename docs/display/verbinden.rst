Verbinden
=========
Direkt mit einer LambdaControl 3 verbinden
------------------------------------------
Um das Display direkt mit einer LambdaControl 3 zu verbinden müssen Sie das Display und die LambdaControl 3 mit einem LAN-Kabel als auch mit dem Displaykabel verbinden. Wenn **nur** die LambdaControl 3 und ein Display verbunden sind - der 2.Anschluss an der LambdaControl 3 ist leer - so wird die LambdaControl 3 mit der :ref:`Fallback IP<fallback-ip>` (192.168.0.11 - die grüne LED an der LambdaControl 3 blinkt schnell) starten. Und das Display zeigt einen :ref:`Timeout<overlay-timeout>` an.
